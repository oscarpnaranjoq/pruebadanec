import ComprasPage from './ComprasPage';

const ComprasConfig = {
    settings: {
        layout: {
            config: {},
        },
    },
    routes: [
        {
            path: 'compras',
            element: <ComprasPage />,
        },
    ],
};

export default ComprasConfig;