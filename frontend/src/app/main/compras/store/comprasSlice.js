import { createAsyncThunk, createEntityAdapter, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';

const initialState = [];

export const findCompras = createAsyncThunk('Tracking/compras/getCompras', async ({ identificacion, fechaInicio, fechaFinal, tipoDocumento }) => {
    const response = await axios.post('/api/tracking/ordenesCompra', { identificacion, fechaInicio, fechaFinal, tipoDocumento })
    const data = await response;
    return data;
});
export const setCompras = createAsyncThunk('Tracking/compras/setCompras', async (compras, { dispatch, getState }) => {
    return compras;
});

const comprasSlice = createSlice({
    name: "compras",
    initialState,
    extraReducers: {
        // [createTutorial.fulfilled]: (state, action) => {
        //     state.push(action.payload);
        // },
        // [retrieveTutorials.fulfilled]: (state, action) => {
        //     return [...action.payload];
        // },
        // [updateTutorial.fulfilled]: (state, action) => {
        //     const index = state.findIndex(tutorial => tutorial.id === action.payload.id);
        //     state[index] = {
        //         ...state[index],
        //         ...action.payload,
        //     };
        // },
        // [deleteTutorial.fulfilled]: (state, action) => {
        //     let index = state.findIndex(({ id }) => id === action.payload.id);
        //     state.splice(index, 1);
        // },
        // [deleteAllTutorials.fulfilled]: (state, action) => {
        //     return [];
        // },
        [findCompras.fulfilled]: (state, action) => {
            return [...action.payload];
        },
    },
});
export const selectCompras = ({ compras }) => compras;

// export const selectCompras = ({ user }) => user;
const { reducer } = comprasSlice;
export default reducer;
