import axios from 'axios';
import trackingServiceConfig from './trackingServiceConfig';

export const listDetalle = async (data) => {
    console.log(data)
    const response = await axios.post(trackingServiceConfig.listDetalle, data);
    const result = await response;
    return result;
}