import i18next from 'i18next';
import ar from './navigation-i18n/ar';
import en from './navigation-i18n/en';
import tr from './navigation-i18n/tr';

i18next.addResourceBundle('en', 'navigation', en);
i18next.addResourceBundle('tr', 'navigation', tr);
i18next.addResourceBundle('ar', 'navigation', ar);
// console.log("+++++++++++++++++++++++");
// console.log(JSON.parse(localStorage.getItem('menu')));
const navigationConfig =
    [
        // {
        //     id: 'tracking',
        //     title: 'Tracking',
        //     type: 'group',
        //     icon: 'verified_user',
        //     children: [
        //         {
        //             id: 'compras',
        //             title: 'Compras',
        //             translate: 'Compras',
        //             type: 'item',
        //             icon: 'heroicons-outline:badge-check',
        //             url: 'compras',
        //             app: 'Compras'
        //         },
        //     ],
        // },
        // {
        //     id: 'auth',
        //     title: 'Auth',
        //     type: 'group',
        //     icon: 'verified_user',
        //     children: [
        //         {
        //             id: 'sign-out',
        //             title: 'Sign out',
        //             type: 'item',
        //             url: 'sign-out',
        //             icon: 'exit_to_app',
        //         },
        //     ],
        // },
    ]

export default navigationConfig;
