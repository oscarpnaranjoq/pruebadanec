import { motion } from 'framer-motion';
import Typography from '@mui/material/Typography';
import FuseSvgIcon from '@fuse/core/FuseSvgIcon';

export default function NoFoundRecords(props) {
    return (
        <div className="w-full flex flex-row min-h-full">
            <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { delay: 0.1 } }}
                className="flex flex-col flex-1 items-center justify-center p-24"
            >
                <FuseSvgIcon className="icon-size-128 mb-16" color="disabled" size={24}>
                    heroicons-outline:x-circle
                </FuseSvgIcon>
                <Typography className="mt-16 text-2xl font-semibold tracking-tight" color="text.secondary">
                    Sin registros
                </Typography>
            </motion.div>
        </div>
    );
}