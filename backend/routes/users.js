const { json } = require('body-parser');
const express = require('express');
router = express.Router();
const { _findAll } = require('../controllers/userController'),
    auth = require('../middleware/auth');

router.get('/', auth, async (req, res, next) => {
    try {
        const users = await _findAll();
        return res.status(200).json(users);
    } catch (e) {
        return res.status(500).json(e.message);
    }
});

module.exports = router;