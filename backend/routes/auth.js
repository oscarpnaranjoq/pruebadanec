const express = require('express');
router = express.Router();
const { _create, _findByUserName } = require('../controllers/userController');
const passport = require('passport');
const jwt = require('jsonwebtoken');



router.post('/sign-in', async (req, res, next) => {
    passport.authenticate('local', { session: false }, function (err, user, info) {
        if (err) return res.status(500).json(err);
        if (!user) return res.status(400).json(info);
        const access_token = jwt.sign(user, process.env.SECRET_KEY, { expiresIn: '1h' });
        return res.status(200).json({
            user: {
                uuid: 'XgbuVEXBU5gtSKdbQRP1Zbbby1i1',
                role: 'personal',
                from: 'example@example.com',
                data: {
                    userId: user.id,
                    displayName: user.usuario,
                    email: '',
                    photoURL: 'assets/images/avatars/usuario.jpg',
                    setting: {
                        layout: {},
                        theme: {}
                    },
                    shortcuts: ["apps.calendar", "apps.mailbox", "apps.contacts"],
                    menu: [{
                        id: 'tracking',
                        title: 'Tracking',
                        type: 'group',
                        icon: 'verified_user',
                        children: [
                            {
                                id: 'compras',
                                title: 'Compras',
                                translate: 'Compras',
                                type: 'item',
                                icon: 'heroicons-outline:badge-check',
                                url: 'compras',
                                app: 'Compras'
                            },
                        ],
                    }, {
                        id: 'auth',
                        title: 'Auth',
                        type: 'group',
                        icon: 'verified_user',
                        children: [
                            {
                                id: 'sign-out',
                                title: 'Cerrar Sesión',
                                type: 'item',
                                url: 'sign-out',
                                icon: 'exit_to_app',
                            },
                        ],
                    }],
                    routes: [{
                        path: "compras"
                    }, {
                        path: "sign-out"
                    }, {
                        path: "sign-in"
                    }, {
                        path: "/"
                    }, {
                        path: "loading"
                    }, {
                        path: "404"
                    }, {
                        path: "*"
                    }]
                }
            },
            access_token,
            expiresIn: 3600,
        });
    })(req, res, next);
});
router.post('/access-token', async (req, res, next) => {
    let jwtSecretKey = process.env.SECRET_KEY;
    try {
        const token = req.body.access_token;
        const verified = jwt.verify(token, jwtSecretKey);
        if (verified) {
            return res.status(200).json({
                user: {
                    uuid: 'XgbuVEXBU5gtSKdbQRP1Zbbby1i1',
                    role: 'personal',
                    from: 'example@example.com',
                    data: {
                        userId: verified.id,
                        displayName: verified.usuario,
                        email: '',
                        photoURL: 'assets/images/avatars/usuario.jpg',
                        setting: {
                            layout: {},
                            theme: {}
                        },
                        shortcuts: ["apps.calendar", "apps.mailbox", "apps.contacts"],
                        menu: [{
                            id: 'tracking',
                            title: 'Tracking',
                            type: 'group',
                            icon: 'verified_user',
                            children: [
                                {
                                    id: 'compras',
                                    title: 'Compras',
                                    translate: 'Compras',
                                    type: 'item',
                                    icon: 'heroicons-outline:badge-check',
                                    url: 'compras',
                                    app: 'Compras'
                                },
                            ],
                        }, {
                            id: 'auth',
                            title: 'Auth',
                            type: 'group',
                            icon: 'verified_user',
                            children: [
                                {
                                    id: 'sign-out',
                                    title: 'Cerrar Sesión',
                                    type: 'item',
                                    url: 'sign-out',
                                    icon: 'exit_to_app',
                                },
                            ],
                        }],
                        routes: [{
                            path: "compras"
                        }, {
                            path: "sign-out"
                        }, {
                            path: "sign-in"
                        }, {
                            path: "/"
                        }, {
                            path: "loading"
                        }, {
                            path: "404"
                        }, {
                            path: "*"
                        }]
                    }
                },
                access_token: token,
                expiresIn: 3600,
            });
        } else {
            // Access Denied
            return res.status(401).send(error);
        }
    } catch (error) {
        // Access Denied
        return res.status(401).send(error);
    }
});
router.post('/signup', async (req, res) => {
    try {
        const findUser = await _findByUserName(req.body.username);
        if (findUser) {
            return res.status(400).json('Usuario registrado');
        }
        const user = await _create(req.body);
        return res.status(201).json({
            status: 'success',
            message: 'Transacción exitosa'
        });
    } catch (e) {
        return res.status(500).json(e.message);
    }
});
module.exports = router;