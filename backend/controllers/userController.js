const { create } = require("../services/users/createService");
const { findByUserName, findAll } = require("../services/users/findService");

async function _create(user) {
    return await create(user);
}

async function _findByUserName(username) {
    return await findByUserName(username);
}

async function _findAll() {
    return await findAll();
}
module.exports = { _create, _findByUserName, _findAll }