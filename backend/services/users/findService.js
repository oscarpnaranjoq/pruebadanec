const db = require("../../models");
const { QueryTypes } = require('sequelize');
async function findByUserName(username) {
    if (!username) throw new Error('Empleado no existe');
    return await db.sequelize.query(
        'SELECT ABAN8 id, TRIM(ABTAX) username, TRIM(ABTAX) clave, TRIM(ABALPH) usuario FROM F0101 t WHERE t.ABTAX = :username',
        {
            replacements: { username: username },
            type: QueryTypes.SELECT,
            plain: true
        }
    ).then(result => {
        return result
    });
}
async function findAll() {
    return await db.USERNODE.findAll({
        attributes: [
            'id',
            'username'
        ]
    });
}
module.exports = { findByUserName, findAll }