const bcrypt = require('bcrypt');
const db = require('../../models');
async function create(user) {
    if (!user.username) throw new Error('Ingrese usuario');
    if (!user.clave) throw new Error('Ingrese contraseña');
    return await db.USERNODE.create({
        ...user,
        clave: user.clave
        // clave: bcrypt.hashSync(user.clave, 10)
    });
}
module.exports = { create }