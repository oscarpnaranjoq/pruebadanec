const db = require("../../models");
const { QueryTypes } = require('sequelize');

async function findOrdenesById(req) {
    return await db.sequelize.query(
        `select scc.phdoco solicitud, scc.phdcto tipo, scc.phkcoo compania,
        CASE 
           WHEN scc.phkcoo='00101' THEN 'DANEC'
           WHEN scc.phkcoo='00105' THEN 'PDA'
           WHEN scc.phkcoo='00106' THEN 'PDE'
           WHEN scc.phkcoo='00107' THEN 'MURRIN'
        END ncompania,
        to_char(to_date(to_char(scc.phtrdj)+1900000,'rrrrddd'),'dd-mm-yyyy') Fecha,
        denv.abalph Destino_Envio,
        scc.phdesc||' '||' Originador : '|| scc.phorby Observaciones
            from proddta.f4301 scc
            join proddta.f0101 denv on scc.phshan=denv.aban8
        where scc.phancr=:identificacion
        and SCc.PhdCTO =:tipoDocumento
        and scc.phtrdj between to_char(to_date(:fechaInicio,'YYYY-MM-DD'),'YYYYDDD')-1900000 and to_char(to_date(:fechaFinal,'YYYY-MM-DD'),'YYYYDDD')-1900000 order by scc.phtrdj
        `,
        {
            replacements: {
                identificacion: req.identificacion,
                tipoDocumento: req.tipoDocumento,
                fechaInicio: req.fechaInicio,
                fechaFinal: req.fechaFinal
            },
            type: QueryTypes.SELECT,
            plain: false
        }
    ).then(result => {
        if (result.length == 0) throw new Error('Sin registros');
        return result
    });
}
async function findDetailOrdenesById(req) {
    return await db.sequelize.query(
        `select NumSolic, TipoSolic, FECTRANS,  CIA, NUMORDEN, TIPOORDEN, SOLICITANTE, DESTINOENVIO,
        FECORDEN, ULESTADO, explicacion, SIGESTADO, Cproveedor, Nproveedor, ITEM , NAriculo, sum(cantidad) cantidad, UM, FECENTPROM,
        UNNEG, initrans,
        case when sum(CANTREC) is null then 0 else sum(cantrec) end CANTREC,
        case when FECRECP is null then to_char(to_date('01/01/1999','dd/mm/yyyy'),'dd-mm-yyyy') else FECRECP end FecRecep, PLAZOENTREGA, FECCAMBIOESTADO,FECENTPROM_REAL,
          numfectra
        from (
        select
        to_number(SC.PDOORN) NumSolic, SC.PDOCTO TipoSolic,
        to_char(to_date(to_char(OS.PDTRDJ)+1900000,'rrrrddd'),'dd-mm-yyyy') FECTRANS,
          to_number(to_char(to_date(to_char(OS.PDTRDJ)+1900000,'rrrrddd'),'yyyymmdd')) numfectra,
        Os.PDKCOO CIA, Os.PDDOCO NUMORDEN, Os.PDDCTO TIPOORDEN,
        SL.ABALPH SOLICITANTE,
        DE.ABALPH DESTINOENVIO,
        to_char(to_date(to_char(Os.PDTRDJ)+1900000,'rrrrddd'),'dd-mm-yyyy') FECORDEN,
        to_char(to_date(to_char(HORDJ)+1900000,'rrrrddd'),'dd-mm-yyyy') FECCAMBIOESTADO,
        Os.PDLTTR ULESTADO, Os.PDNXTR SIGESTADO, pr.aban8 Cproveedor,
        PR.ABALPH Nproveedor,  Os.PDLITM ITEM, Os.PDDSC1 NAriculo,  Os.PDUORG/100 cantidad, Os.PDUOM UM,
        case when /*HOASTS='3N' AND*/ Os.PDLTTR>='280'-- and OC.PDNXTR='999'
        then
          to_char(to_date(to_char(HORDJ)+1900000,'rrrrddd') + (to_date(to_char(Os.PDPDDJ)+1900000,'rrrrddd') - to_date(to_char(Os.PDTRDJ)+1900000,'rrrrddd') ) )
          ELSE
            CASE WHEN  Os.PDLTTR='230' THEN-- ORDEN SIN APROBAR
             '01/01/1900'
          ELSE
           case when Os.PDLTTR<'280' THEN
               to_char(to_date(to_char(Os.PDPDDJ)+1900000,'rrrrddd') + (to_date(to_char(Os.PDPDDJ)+1900000,'rrrrddd') - to_date(to_char(Os.PDTRDJ)+1900000,'rrrrddd') ) )
              ELSE
                  '01/01/1900'
                END
                 end
        end  FECENTPROM,
        Os.PDMCU UNNEG, Os.PDTORG initrans,
        PRUREC/100 CANTREC,
        case when PRRCDJ=0 THEN to_char(TO_DATE('01/01/1999','DD/MM/YYYY'),'dd-mm-yyyy') ELSE
        to_char(to_date(to_char(PRRCDJ)+1900000,'rrrrddd'),'dd-mm-yyyy') END FECRECP,
        se.explicacion,  to_char(to_date(to_char(Os.PDPDDJ)+1900000,'rrrrddd'),'dd-mm-yyyy') FECENTPROM_REAL, to_date(to_char(Os.PDPDDJ)+1900000,'rrrrddd') - to_date(to_char(Os.PDTRDJ)+1900000,'rrrrddd') PLAZOENTREGA
        from
           PRODDTA.F4311 SC
        LEFT OUTER JOIN PRODDTA.F4311 OS ON os.PDOORN=sc.PDDOCO AND  os.PDOCTO=sc.PDDCTO AND os.PDOKCO=sc.PDKCOO --AND SC.PDOCTO='OR'
        left outer JOIN PRODDTA.F0101 PR ON os.PDAN8=PR.ABAN8
        left outer  JOIN PRODDTA.F0101 DE ON os.PDSHAN=DE.ABAN8
        left outer   JOIN PRODDTA.F0101 SL ON os.PDANCR=SL.ABAN8
           join seguimientoestadoorden se on os.pdlttr=se.lttr and os.pdnxtr=se.nxtr and os.pddcto=se.tipoorden
           JOIN PRODDTA.f4209 ON  HOKCOO=os.PDKCOO AND HODOCO=os.PDDOCO AND HODCTO=os.PDDCTO and HOASTS in ('3N','2R')
        LEFT OUTER JOIN PRODDTA.F43121 ON PRKCOO=Os.PDKCOO AND PRDOCO=Os.PDDOCO AND PRDCTO=Os.PDDCTO AND PRITM=Os.PDITM AND PRMATC=1
        where 
        sc.pdkcoo =:compania
        and sc.pdoorn=:idCabecera
        and sc.pdocto=:tipoDocumento
          AND  Os.PDTRDJ >= to_number(to_char((add_months(trunc(sysdate,'year'),-0)),'yyyyddd'))-1900000
        AND  Os.PDTRDJ <=to_number(to_char(((trunc(sysdate-1))),'yyyyddd'))-1900000
          )
        group by NumSolic, TipoSolic, FECTRANS, CIA, NUMORDEN, TIPOORDEN, SOLICITANTE, DESTINOENVIO,
        FECORDEN, ULESTADO, SIGESTADO, Nproveedor, ITEM , NAriculo, UM,FECENTPROM,
        UNNEG, initrans, FECRECP, explicacion,PLAZOENTREGA, FECCAMBIOESTADO,FECENTPROM_REAL, numfectra, Cproveedor
        `,
        {
            replacements: {
                compania: req.compania,
                idCabecera: req.idCabecera,
                tipoDocumento: req.tipoDocumento
            },
            type: QueryTypes.SELECT,
            plain: false
        }
    ).then(result => {
        if (result.length == 0) throw new Error('Sin registros');
        return result
    });
}
module.exports = { findOrdenesById, findDetailOrdenesById }