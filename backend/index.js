
// const db = require('./models');
require('dotenv').config();
const { QueryTypes } = require('sequelize');
const port = process.env.PORT || 3001,
    express = require('express'),
    app = express(),
    db = require('./models'),
    cors = require('cors'),
    bodyParser = require('body-parser'),
    passport = require('passport'),
    localStrategy = require('./passport/local'),
    JWTStrategy = require('./passport/jwt'),
    SDM = require('./crons/tasks');

// var cron = require('node-cron');
// cron.schedule('* * * * *', () => {
//     console.log('running a task every minute');
// });

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

passport.use('local', localStrategy);
passport.use('jwt', JWTStrategy);
app.use(passport.initialize());

app.use('/api/auth', require('./routes/auth'));
app.use('/api/tracking', require('./routes/tracking'));
// app.use('/api/encuesta', require('./routes/encuestas'));

app.listen(port, () => {
    console.log(`Servidor corriendo en el puerto: ${port}...`);
});
db.sequelize.authenticate()
    .then(() => console.log('Conexion exitosa!!!'))
    .catch((e) => console.log(`Error => ${e}`))

// var exec = require('./crons/tasks')