const LocalStrategy = require('passport-local').Strategy;
const { _findByUserName } = require('../controllers/userController');
const bcrypt = require('bcrypt');

module.exports = new LocalStrategy({
    usernameField: 'username',
    passwordField: 'clave',
    passReqToCallback: true,
    session: false
}, async (req, username, password, done) => {
    try {
        const user = await _findByUserName(req.body.username);
        if (!user) return done(null, false, 'Usuario o contraseña incorrectos');
        // const match = bcrypt.compareSync(password, user.clave);
        if (password !== user.CLAVE) return done(null, false, 'Usuario o contraseña incorrectos');
        return done(null, {
            usuario: user.USUARIO,
            username: user.USERNAME,
            id: user.ID
        });
    } catch (e) {
        done(e);
    }
});